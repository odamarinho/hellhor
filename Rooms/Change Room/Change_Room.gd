extends Node

func _on_Area2D_body_entered(body):
	if body.is_in_group("player"):
		GameController.change_room()
		GameController.n_passed_rooms += 1
	
	if body.is_in_group("enemie"):
		body.speedy *= -1
