extends Node

func init():
	if GameController.armor >= 2 or GameController.armor == 0:
		return false
	
	if GameController.armor == 1:
		GameController.armor += 1
		return true
	
	GameController.armor += 2
	return true
