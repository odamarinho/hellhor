extends KinematicBody2D

export var speed = 250

var move = Vector2()
var direction = Vector2()

func init(var dir : Vector2, var pos):
	direction = dir
	self.position.x = pos.x + dir.x * 25
	self.position.y = pos.y + dir.y * 25
	return true

func _ready():
	
	if direction.x == 1:
		self.rotate(PI / 2)
	
	elif direction.x == -1:
		self.rotate(-(PI / 2))
		$Sprite.flip_h = true
	
	if direction.y == 1:
		self.rotate(PI)

func _process(delta):
	move.x = direction.x * speed * delta
	move.y = direction.y * speed * delta
	
	var col = move_and_collide(move)
	
	if col != null:
		if col.collider.is_in_group("enemie"):
			col.collider.queue_free()
		self.queue_free()
