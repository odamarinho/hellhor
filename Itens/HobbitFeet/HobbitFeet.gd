extends Node

func init():
	if GameController.multiplier == 2:
		return false
	GameController.multiplier = 2
	return true
