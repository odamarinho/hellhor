extends KinematicBody2D

export var speed = 80
export var speed_multiplier = 1

var move = Vector2()
var direction = Vector2(1, 0)

func delay_hited():
	$AnimatedSprite.modulate = Color(1, 1, 1, 0.3)
	GameController.player_invincible = true
	$delay_hited_timer.start(2)

func _ready():
	if GameController.armor >= 1:
		$AnimatedSprite.animation = "idle" + str(GameController.armor)
	
	speed_multiplier = GameController.speed_multiplier
	$AnimatedSprite.speed_scale = GameController.speed_multiplier
	

func _input(event):
	if event.is_action_pressed("use_item"):
		if GameController.equiped_item_one == null:
			return
		
		var aux = GameController.equiped_item_one.instance()
		var can_instance = aux.init(direction, self.position)
		if can_instance: 
			get_parent().add_child(aux)
			GameController.bag["0"] -= 1
			get_node("/root/Node2D/hud_node/items_hud").set_item_text(1, str(GameController.bag["0"]))
			if GameController.bag["0"] <= 0:
				GameController.equiped_item_one = null


func _process(delta):
	if Input.is_action_pressed("ui_up"):
		direction = Vector2(0, -1)
		move.y -= speed * delta * speed_multiplier
		$AnimatedSprite.animation = "run" + str(GameController.armor)
	
	elif Input.is_action_pressed("ui_down"):
		direction = Vector2(0, 1)
		move.y += speed * delta * speed_multiplier
		$AnimatedSprite.animation = "run" + str(GameController.armor)
	
	elif Input.is_action_pressed("ui_left"):
		direction = Vector2(-1, 0)
		move.x -= speed * delta * speed_multiplier
		$AnimatedSprite.animation = "run" + str(GameController.armor)
		$AnimatedSprite.flip_h = true
	
	elif Input.is_action_pressed("ui_right"):
		direction = Vector2(1, 0)
		move.x += speed * delta * speed_multiplier
		$AnimatedSprite.animation = "run" + str(GameController.armor)
		$AnimatedSprite.flip_h = false
		
	if Input.is_action_just_released("ui_right"):
		$AnimatedSprite.animation = "idle" + str(GameController.armor)
	
	elif Input.is_action_just_released("ui_left"):
		$AnimatedSprite.animation = "idle" + str(GameController.armor)
	
	elif Input.is_action_just_released("ui_up"):
		$AnimatedSprite.animation = "idle" + str(GameController.armor)
	
	elif Input.is_action_just_released("ui_down"):
		$AnimatedSprite.animation = "idle" + str(GameController.armor)
	
	var col = move_and_collide(move)
	
	if col != null:
		if col.collider.is_in_group("enemie"):
			GameController.kill_player(true)
	
	move.x = 0
	move.y = 0
	



func _on_delay_hited_timer_timeout():
	$AnimatedSprite.modulate = Color(1, 1, 1, 1)
	GameController.player_invincible = false
