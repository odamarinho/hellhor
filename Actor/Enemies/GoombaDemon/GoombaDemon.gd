extends KinematicBody2D

export var speed = 50
var move = Vector2()

func set_speed():
	pass


func _physics_process(delta):
	move.x = speed * delta
	#move.y = speed * delta

	var col = move_and_collide(move)

	if col != null:
		
		if col.collider.name == "wall":
			speed *= -1
			$AnimatedSprite.flip_h = !$AnimatedSprite.flip_h

