extends KinematicBody2D

var prev_posx = 0

func _process(delta):
	$AnimatedSprite.flip_h = prev_posx > global_position.x
	
	prev_posx = global_position.x

func _ready():
	prev_posx = global_position.x