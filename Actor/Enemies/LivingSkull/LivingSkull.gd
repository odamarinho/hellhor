extends KinematicBody2D

export var speedx = 3000
export var speedy = 3000
export var begin_up = false
export var begin_left = false

var move = Vector2()

const NORMAL = Vector2(0, -1)

func _ready():
	set_orientation()
	
	if begin_up:
		speedy *= -1
	
	if begin_left:
		speedx *= -1
		$Sprite.flip_h = !$Sprite.flip_h

func set_speed():
	pass

func set_orientation():
	if GameController.dice.roll(1, 2) == 1:
		begin_up = true
	
	if GameController.dice.roll(1, 2) == 1:
		begin_left = true
	


func _physics_process(delta):
	move.x = speedx * delta
	move.y = speedy * delta
	
	move_and_slide(move, NORMAL)
	
	
	if get_slide_count() > 0:
		var col = get_slide_collision(0)
			
		if is_on_floor() or is_on_ceiling():
			speedy *= -1
			
		else:
			speedx *= -1
			$Sprite.flip_h = !$Sprite.flip_h
		