extends Area2D

onready var timer = $Timer

var speed = 1.0
var player_is_in = false
var is_hole = false
var i = float(2)

func _ready():
	var texture_id = GameController.dice.roll(1, 5)
	$Sprite.texture = load("res://Traps/BrokenFloor/Sprites/bf_" + str(texture_id) + ".png")
	set_speed()
	set_process(false)

func _on_Area2D_body_entered(body):
	if body.is_in_group("player") and !is_hole:
		player_is_in = true
		timer.start(speed)
		set_process(true)
	
	elif body.is_in_group("player") and is_hole:
		GameController.kill_player(false)


func _on_Area2D_body_exited(body):
	if body.is_in_group("player") and !is_hole:
		player_is_in = false


func set_speed():
	speed -= float(1) * 0.1

func _on_Timer_timeout():
	if player_is_in:
		GameController.kill_player(false)
	
	$Sprite.texture = null
	is_hole = true
	timer.stop()

func _process(delta):
	self.position = Vector2(self.position.x + i, self.position.y)
	i *= float(-1)
	