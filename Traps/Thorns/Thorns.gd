extends Area2D

onready var timer = $Timer
onready var anim = $AnimatedSprite
export var speed = 1.0
export(bool) var start_up = true

var is_up = true
var player_is_in = false

func _ready():
	if !start_up:
		change_state()
	
	set_speed()
	timer.start(speed)

func _on_Timer_timeout():
	change_state()

func change_state():
	if is_up:
		anim.play("change", true)
	
	else:
		anim.play("change", false)
	
	is_up = !is_up
	if is_up and player_is_in:
		GameController.kill_player(true)


func _on_AnimatedSprite_animation_finished():
	if anim.animation == "change":
		if is_up:
			anim.animation = "thorns_up"
	
		else:
			anim.animation = "thorns_down"


func _on_Area2D_body_entered(body):
	if is_up and body.is_in_group("player"):
		GameController.kill_player(true)
	
	if body.is_in_group("player"):
		player_is_in = true
	


func _on_Area2D_body_exited(body):
	if body.is_in_group("player"):
		player_is_in = false

func set_speed():
	speed -= float(1) * 0.1
