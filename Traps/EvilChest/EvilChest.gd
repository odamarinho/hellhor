extends Node2D

var opened = false

func _on_Area2D_body_entered(body):
	if(body.is_in_group("player") and !opened):
		$StaticBody2D/AnimatedSprite.animation = "opeaning"
		
		var perc_to_monster = GameController.dice.roll(5, 20)
		
		if GameController.dice.roll(1, 100) < perc_to_monster:
			turn_monster()
		
		else:
			opened = true
			GameController.give_coins(GameController.dice.roll(2, 6))


func _on_AnimatedSprite_animation_finished():
	if($StaticBody2D/AnimatedSprite.animation == "opeaning"):
		$StaticBody2D/AnimatedSprite.animation = "open"


func turn_monster():
	var monster = load("res://Actor/Enemies/LivingSkull/LivingSkull.tscn")
	var monster_instance = monster.instance()
	monster_instance.position = self.position
	get_parent().add_child(monster_instance)
	self.queue_free()