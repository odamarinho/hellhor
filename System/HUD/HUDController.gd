extends Node2D

onready var ep = $end_panel
onready var items_hud = $items_hud
onready var passed_room_label = $Node2D/pr_label
onready var ppause = $panel_pause

func _ready():
	ep.visible
	ep.hide()
	ppause.hide()
	
	show_items_hud()
	passed_room_label.text = str(GameController.n_passed_rooms)

func show_panel():
	ep.show()
	$end_panel/colected_coins.text = "MOEDAS COLETADAS: " + str(GameController.match_coins)
	$end_panel/rooms_passed.text = "SALAS PASSADAS: " + str(GameController.n_passed_rooms)
	$end_panel/ItemList.grab_focus()
	$end_panel/ItemList.select(0)


func _on_ItemList_item_activated(index):
	if index == 0:
		GameController.init_game()
	
	elif index == 1:
		get_tree().change_scene("res://System/Shop/shop.tscn")
	
	elif index == 2:
		get_tree().change_scene("res://Menu.tscn")

func show_items_hud():
	items_hud.set_item_text(0, str(GameController.match_coins))
	for item in GameController.items:
		if GameController.bag.has(item.id):
			if GameController.can_show_hud.has(item.id):
				items_hud.add_item(str(GameController.bag[item.id]), item.icon)


func _input(event):
	if event.is_action_pressed("ui_cancel"):
		if !get_tree().paused:
			ppause.show()
			get_tree().paused = true
		
		else:
			ppause.hide()
			get_tree().paused = false