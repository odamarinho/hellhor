extends PathFollow2D

#onready var path = get_node("PathFollow2D")
export var orientation = 1
export var speed = 150

func _process(delta):
	set_offset(get_offset() + speed * delta * orientation)

func _ready():
	set_process(true)
	set_offset(GameController.dice.roll(1, 1500))