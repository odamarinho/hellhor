extends Node

func _ready():
	var i = 0
	for item in $Items.get_children():
		GameController.items.append(load("res://Itens/Item.gd").new())
		GameController.items[i].id = item.id
		GameController.items[i].nome = item.nome
		GameController.items[i].info = item.info
		GameController.items[i].icon = item.icon
		GameController.items[i].price = item.price
		GameController.items[i].object_path = item.object_path
		i += 1
	
	$ItemList.grab_focus()
	$ItemList.select(0)


func _on_ItemList_item_activated(index):
	if index == 0:
		GameController.init_game()
	
	elif index == 1:
		get_tree().change_scene("res://System/Shop/shop.tscn")
	
	elif index == 3:
		get_tree().quit()
