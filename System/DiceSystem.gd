extends Node

func roll(n_dices, n_faces, mod=0):
	var result = 0
	for i in range(n_dices):
		var rolls = RandomNumberGenerator.new()
		rolls.randomize()
		result += rolls.randi_range(1, n_faces)
	return result + mod

	
