extends Control

var precos = []
var texts = []


onready var shop = $Panel/shop
onready var coins = $Panel/coins_label

func _ready():
	coins.text = str(GameController.coins)
	
	var i = 0
	for item in GameController.items:
		shop.add_item(item.nome, item.icon)
		texts.append(item.info)
		precos.append(item.price)
		if item.id == "4" and GameController.bag["1"] <= 0:
			#shop.set_item_selectable(i, false)
			shop.set_item_disabled(i, true)
		i += 1
	
	shop.grab_focus()
	shop.select(0)
	$Panel/info_item.text = update_item_text(0)


func _on_shop_item_selected(index):
	$Panel/info_item.text = update_item_text(index)

func _on_shop_item_activated(index):
	if GameController.coins >= precos[index]:
		GameController.coins -= precos[index]
		GameController.bag[str(index)] += 1
		_on_shop_item_selected(index)
		coins.text = str(GameController.coins) 
		GameController.database.update_items()
		GameController.database.update_coins()
		
		if index == 1:
			#shop.set_item_selectable(4, true)
			shop.set_item_disabled(4, false)

func update_item_text(var i):
	if GameController.bag.has(str(i)):
		return "Voce possui " + str(GameController.bag[str(i)]) + " deste Item.\n\nPRECO: " + str(precos[i]) + " moedas.\n\n" + texts[i]
	
	return ""

func _input(event):
	if event.is_action_pressed("ui_cancel"):
		get_tree().change_scene("res://Menu.tscn")
	
