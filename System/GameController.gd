extends Node

onready var dice = preload("res://System/DiceSystem.gd").new()
onready var database = preload("res://System/DataBase.gd").new()

var n_rooms = 6
var armor = 0
var speed_multiplier = 1
var player_invincible = false
var coins = 0
var match_coins = 0
var passed_rooms = []
var n_passed_rooms = 0

var items = []
var bag = {}
var can_show_hud = {}
var equiped_item_one
var equiped_item_two

var multiplier = 1

func give_coins(var n=1):
	match_coins += n * multiplier
	get_node("/root/Node2D/hud_node/items_hud").set_item_text(0, str(match_coins))


func change_room():
	if passed_rooms.size() == n_rooms:
		passed_rooms.clear()
	var new_room_id = dice.roll(1, n_rooms)
	
	while passed_rooms.has(new_room_id):
		new_room_id = dice.roll(1, n_rooms)
	
	passed_rooms.append(new_room_id)
	get_tree().change_scene("res://Rooms/room"+str(new_room_id)+".tscn")

func kill_player(armor_can_save):
	if player_invincible:
		return
	
	if armor_can_save and armor > 0:
		var player = get_node("/root/Node2D/Player/KinematicBody2D")
		player.delay_hited()
		armor -= 1
		get_node("/root/Node2D/Player/KinematicBody2D/AnimatedSprite").animation = "idle" + str(armor)
		print("TIROU ARMOR!!")
	
	else:
		get_node("/root/Node2D/Player").queue_free()
		get_node("/root/Node2D/hud_node/").show_panel()
		armor = 0
		speed_multiplier = 1
		coins += match_coins
		database.update_coins()
		database.update_items()
		clear_hud()
	

func init_game():
	match_coins = 0
	multiplier = 1
	n_passed_rooms = 0
	get_tree().change_scene("res://Rooms/room_initial.tscn")
	
	for item in items:
		if bag.has(item.id):
			if bag[item.id] > 0:
				if item.id == "0":
					equiped_item_one = load(item.object_path)
					can_show_hud[item.id] = true
					continue
				
		
				print(item.object_path)
				var item_obj = load(item.object_path)
				var item_instance = item_obj.instance()
				var b = item_instance.init()
				if b:
					get_parent().add_child(item_instance)
					#bag[item.id] -= 1
					can_show_hud[item.id] = true

func _ready():
	database.load_game()
	VisualServer.set_default_clear_color(Color(0.15, 0.15, 0.15, 1))
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	

func clear_hud():
	var i = 1
	for item in can_show_hud:
		if item:
			bag[str(i)] -= 1
		i += 1
	
	can_show_hud.clear()