extends Node

const database_path = "res://System/database.json"

var database = {}

func save_file(path):
	var file = File.new()
	file.open(path, file.WRITE)
	file.store_string(to_json(database))
	file.close()

func load_file():
	var file = File.new()
	file.open(database_path, file.READ)
	database = parse_json(file.get_as_text())
	file.close()
	
func update_coins():
	database["coins"] = GameController.coins
	save_file(database_path)

func load_game():
	load_file()
	GameController.coins = database["coins"]
	GameController.bag = database["bag"]

func update_items():
	database["bag"] = GameController.bag
	save_file(database_path)

func update_achievement(achv_id):
	#TO DO
	pass

	